const { User } = require("../models");

module.exports = {
  create(createArgs) {
    return User.create(createArgs);
  },

  update(id, updateArgs) {
    return User.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return User.destroy(id);
  },

  find(id) {
    return User.findByPk(id);
  },

  findAll() {
    return User.findAll({ attributes: { exclude: ['password'] } });
  },

  getTotalUser() {
    return User.count();
  },


  registerNewUser(createArgs) {
    return User.create(createArgs);
  },

  async testingUser(email) {
    const user = await User.findOne({
      where: {
        email: email,
      },
    });
    return user;
  },


};