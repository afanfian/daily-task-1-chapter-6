const bcrypt = require('bcrypt');

const userRepository = require("../repositories/userRepository");

module.exports = {
  create(requestBody) {
    return userRepository.create(requestBody);
  },

  update(id, requestBody) {
    return userRepository.update(id, requestBody);
  },

  delete(id) {
    return userRepository.delete(id);
  },

  async list() {
    try {
      const posts = await userRepository.findAll();
      const postCount = await userRepository.getTotalUser();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return userRepository.find(id);
  },
  // Untuk register user
  async register(requestBody) {
    const hash = bcrypt.hashSync(requestBody.password, 10);
    const { firstName, lastName, email } = requestBody;
    const reqBody = {
      firstName,
      lastName,
      email,
      password: hash,
    };
    const testingUser = await userRepository.testingUser(email);
    if (testingUser) {
      throw new Error(`Email Already Exists`);
    }
    return userRepository.registerNewUser(reqBody);
  },
  // Untuk Login
  async login(requestBody) {
    const testingUser = await userRepository.testingUser(requestBody.email);
    if (!testingUser) {
      throw new Error(`Email Doesn't Exists`);
    }
    return testingUser;
  },
};
